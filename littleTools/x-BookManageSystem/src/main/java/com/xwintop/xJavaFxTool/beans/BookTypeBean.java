package com.xwintop.xJavaFxTool.beans;

import lombok.Data;

@Data
public class BookTypeBean {
    private int bookTypeId;
    private String bookTypeName;
    private String bookTypeDescription;
}
